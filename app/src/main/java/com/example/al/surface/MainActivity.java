package com.example.al.surface;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity implements View.OnClickListener {

    DrawView drawView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawView = findViewById(R.id.drawView);
        findViewById(R.id.button_left).setOnClickListener(this);
        findViewById(R.id.button_up).setOnClickListener(this);
        findViewById(R.id.button_down).setOnClickListener(this);
        findViewById(R.id.button_right).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_left:
                drawView.drawThread.setDx(-5);
                break;
            case R.id.button_up:
                drawView.drawThread.setDy(-5);
                break;
            case R.id.button_down:
                drawView.drawThread.setDy(5);
                break;
            case R.id.button_right:
                drawView.drawThread.setDx(5);
                break;
        }
    }
}
