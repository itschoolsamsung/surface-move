package com.example.al.surface;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class DrawView extends SurfaceView implements SurfaceHolder.Callback {

    public DrawThread drawThread;

    public DrawView(Context context) {
        super(context);

        getHolder().addCallback(this);
    }

    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);

        getHolder().addCallback(this);
    }

    public DrawView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        getHolder().addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        drawThread = new DrawThread(getContext(), getHolder());
        drawThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        drawThread.requestStop();
        boolean retry = true;

        while (retry) {
            try {
                drawThread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        drawThread.setX(event.getX());
        drawThread.setY(event.getY());

        return super.onTouchEvent(event);
    }

    public class DrawThread extends Thread {

        private SurfaceHolder surfaceHolder;
        private volatile boolean running = true;
        private Paint paint = new Paint();
        private float x = 100;
        private float y = 100;
        private float dx = 0;
        private float dy = 0;

        DrawThread(Context context, SurfaceHolder surfaceHolder) {
            this.surfaceHolder = surfaceHolder;
        }

        void setX(float x) {
            this.x = x;
        }

        void setY(float y) {
            this.y = y;
        }

        void setDx(float dx) {
            this.dx = dx;
        }

        void setDy(float dy) {
            this.dy = dy;
        }

        void requestStop() {
            running = false;
        }

        @Override
        public void run() {
            while (running) {
                Canvas canvas = surfaceHolder.lockCanvas();
                if (canvas != null) {
                    try {
                        canvas.drawARGB(255, 0, 0, 50);
                        paint.setColor(Color.RED);
                        canvas.drawCircle(x, y, 50, paint);
                        x += dx;
                        y += dy;
                        if (x <= 0) {
                            x = 0;
                            dx = 0;
                        }
                        if (x >= canvas.getWidth()) {
                            x = canvas.getWidth();
                            dx = 0;
                        }
                        if (y <= 0) {
                            y = 0;
                            dy = 0;
                        }
                        if (y >= canvas.getHeight()) {
                            y = canvas.getHeight();
                            dy = 0;
                        }
                    } finally {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }
}
